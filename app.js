/*
 * Core launcher of apipanda.
 * Usage: node app.js
 */

'use strict';

const rootpath = process.cwd() + '/',
    nodepath = require('path'),
    express = require('express'),
    bodyParser = require('body-parser'),
    responseTime = require('response-time'),
    apipanda = require(nodepath.join(rootpath, 'lib/apipanda')),
    everyauth = require('everyauth');

const path = rootpath,
    port = (process.env.PORT && parseInt(process.env.PORT)) || 3000;

function bootAPI(next) {
    var app = express();
    app.path = function() {
        return path
    };

	apipanda.init(app, function() {
        app.use(bodyParser);
        app.use(express.json({}))
        app.use(responseTime());
	    next(app);
    });
}

exports.boot = function(next) {
	bootAPI(next);
}

if (!module.parent) {
	exports.boot(function(app) {
		if (app) {
			var out = app.listen(port, function() {
				console.log("Apipanda is running on port: " + port);
			})
		}
	})
}