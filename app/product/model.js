'use strict';

const mongoose = require('mongoose');

var ProductSchema = new mongoose.Schema({
    description: {
        type: String,
        required: true,
        trim: true
    },
    cost: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    stock: {
        type: Number,
        required: true
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    }
})

var Product = module.exports = mongoose.model('Product', ProductSchema);