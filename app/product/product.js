'use strict';

const rootpath = process.cwd() + '/',
	nodepath = require('path'),
	express = require('express'),
	auth = require(nodepath.join(rootpath, 'lib/core/auth')),
	Product = require('./model');

var productApi = module.exports = {
	init: init,
	list: list,
	show: show,
	create: create,
	update: update,
	remove: remove 
}

function init(cb) {
	productApi.router = express.Router();
	productApi.router.use(auth.jwtMiddleware);
	productApi.router.get('/', productApi.list);
	productApi.router.get('/:id', productApi.show);
	productApi.router.post('/', productApi.create);
	productApi.router.delete('/:id', productApi.remove);
	productApi.router.put('/:id', productApi.update);
	if (cb) cb();
}

function list(req, res) {
	Product.find({ userId: req.decoded.id }, function(err, products) {
		// since we have very little amount of endpoints,
		// I decided not to create a new error handling middleware
		if (err) {
			res.status(500);
			res.json({success: false, message: err});
		}

		// returns your paid money for all your products 
		if (req.query.showPaidAmount) {
			var amount = 0;
			products.forEach(function(product) {
				amount += product.stock * product.cost;
			});
			return res.json({amount: amount});
		}

		// returns amount of money if you sold all your products
		if (req.query.showSoldAllAmount) {
			var amount = 0;
			products.forEach(function(product) {
				amount += product.price * product.stock;
			})
			return res.json({amount: amount});
		}
		res.json(products);
	})
}

function show(req, res) {
	Product.findOne({ _id:  req.params.id, userId: req.decoded.id }, function(err, product) {
		if (err) {
			res.status(500);
			res.json({success: false, message: err});
		}

		// returns amount of money you paid for your product in stock
		if (req.query.showPaidAmount) {
			return res.json({amount: product.cost * product.stock});
		}

		// returns amount of money if you sold your product in stock
		if (req.query.showSoldAllAmount) {
			return res.json({amount: product.price * product.stock});
		}
		res.json(product);
	});
}

function create(req, res) {
	req.body.userId = req.decoded.id;
	Product.create(req.body, function(err) {
		if (err) {
			res.status(500);
			res.json({success: false, message: err});
		}
	});
	res.send({success: true, message: "created product successfully"});
}

function remove(req, res) {
	Product.findOneAndRemove({ _id: req.params.id, userId: req.decoded.id }, function(err) {
		if (err) {
			res.status(500);
			res.json({success: false, message: err});
		}
	});
	res.send(204);
}

function update(req, res) {
	Product.findOneAndUpdate({ _id: req.params.id, userId: req.decoded.id }, req.body, function(err) {
		if (err) {
			res.status(500);
			res.json({success: false, message: err});
		}
	})
	res.send(204);
}