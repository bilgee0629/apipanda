'use strict';

const mongoose = require('mongoose'),
    passwordHash = require('password-hash');

var UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        trim: true,
        index: {unique: true}
    },
    password: {
        type: String,
        required: true
    }
});

UserSchema.pre('save', function(next) {
    var user = this;
    if (!user.isModified('password')) return next();
    user.password = passwordHash.generate(user.password);
    next();
});

UserSchema.methods.verify = function(candidatePassword, cb) {
    var user = this;
    return passwordHash.verify(candidatePassword, user.password)
};

var User = module.exports = mongoose.model('User', UserSchema);