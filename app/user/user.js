'use strict';

const rootpath = process.cwd() + '/',
	nodepath = require('path'),
	express = require('express'),
	jwt = require('jsonwebtoken'),
	auth = require(nodepath.join(rootpath, '/lib/core/auth')),
	apipanda = require(nodepath.join(rootpath, 'lib/apipanda')),
	User = require('./model');

var userApi = module.exports = {
	init: init,
	create: create,
	getToken: getToken
}

function init(cb) {
	userApi.router = express.Router();
	userApi.router.post('/gettoken', userApi.getToken);
	userApi.router.post('/', userApi.create);
	if (cb) cb();
}

function create(req, res) {
	User.create(req.body, function(err) {
		if (err) {
			res.status(500);
			res.json({success: false, message: err});
		}
	});
	res.send(204);
}

function getToken(req, res) {
	User.findOne({ username: req.body.username }, function(err, user) {
		if (err) {
			res.status(500);
			res.json({success: false, message: err});
		}

		if (user.verify(req.body.password)) {
			var token = auth.signToken(user.id);
			return res.json({success: true, message: 'Successfully logged in', token: token});
		}
	});
}