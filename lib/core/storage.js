'use strict';

const mongoose = require('mongoose');

module.exports.connectMongoose = function() {
    return mongoose.connect("mongodb://127.0.0.1:27017/apipanda");
}