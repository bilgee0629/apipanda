/*
 * Core libraries of apipanda
 */

'use strict';

const rootpath = process.cwd() + '/',
	nodepath = require('path'),
	fs = require('fs'),
	bodyParser = require('body-parser'),
	mongoose = require('mongoose'),
	storage = require(nodepath.join(rootpath, 'lib/core/storage')),
	userApi = require(nodepath.join(rootpath, 'app/user/user')),
	productApi = require(nodepath.join(rootpath, 'app/product/product'));

var apipanda = module.exports = {
	init: init,
	router: loadRoutes
};

loadCoreLibs(apipanda);

function loadCoreLibs(apipanda) {
	fs.readdirSync(__dirname + '/core').forEach(function (library) {
		let isLibrary = library.split(".").length > 0 && library.split(".")[1] === 'js',
	      	libName = library.split(".")[0].toLowerCase();
	    if (isLibrary) {
	      	apipanda[libName] = require(__dirname + '/core/' + library);
	    }
	})
}

function init(app, initCallback) {
	apipanda.app = app;
	apipanda.config = app.config;
	storage.connectMongoose();
	userApi.init();
	productApi.init();
	apipanda.router(app);
	initCallback();
}

function loadRoutes(app) {
	app.use(bodyParser.urlencoded({extended: false}));
	app.use(bodyParser.json());

	app.use('/users', userApi.router);
	app.use('/products', productApi.router);
}